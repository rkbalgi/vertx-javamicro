FROM envoyproxy/envoy:latest
#FROM openjdk:9
RUN apt-get update && apt-get install -y openjdk-8-jdk && apt-get install -y curl
RUN mkdir -p /usr/work/javamicro
RUN mkdir -p /etc/envoy
COPY ./target/*.jar /usr/work/javamicro/
COPY ./config/envoy.yaml /etc/envoy/envoy.yaml
COPY ./start_procs.sh /usr/work/javamicro
WORKDIR /usr/work/javamicro
#CMD ./start_procs.sh
#ENTRYPOINT ./start_procs.sh 
RUN echo "Hello Docker!!!"
ENTRYPOINT ./start_procs.sh 
