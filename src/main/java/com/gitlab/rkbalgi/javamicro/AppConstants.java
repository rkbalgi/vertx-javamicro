package com.gitlab.rkbalgi.javamicro;

public class AppConstants {

  public static final String APP_CONFIG_FILE = "app_config.json";
  public static final String APP_CONFIG = "app_config";
  public static final String SRVC1 = "srvc1";
  public static final String BE_SRVC_2 = "be_srvc2";
  static final String ENVOY_LISTENER_URL_BE_SRVC2 = "envoy.be_srvc2_listener_url";
  public static final String USE_HTTP2 = "use_http2";
}
