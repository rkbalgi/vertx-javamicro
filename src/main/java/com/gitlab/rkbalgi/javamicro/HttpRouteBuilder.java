package com.gitlab.rkbalgi.javamicro;

import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.core.buffer.Buffer;
import io.vertx.reactivex.core.http.HttpClient;
import io.vertx.reactivex.ext.web.Router;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpRouteBuilder {

  private static final Logger LOG = LogManager.getLogger(HttpRouteBuilder.class);
  private final Router router;

  public HttpRouteBuilder(Router router) {
    this.router = router;
  }

  public void addRoutes() {

    final AtomicLong backEndServiceCallCount = new AtomicLong(0);

    Vertx vertx = Vertx.currentContext().owner();
    HttpClient httpClient = vertx.createHttpClient(new HttpClientOptions());

    JsonObject config = vertx.getOrCreateContext().get(AppConstants.APP_CONFIG);

    String envType = config.getString("srvc_type");
    if (envType != null) {
      LOG.debug("srvc_type:: = {}", envType);
    } else {
      envType = AppConstants.SRVC1;
      LOG.error("srvc_type unspecified, will default to srvc1", envType);
    }

    LOG.debug("Registering routes .. ");



    /*router.route().handler(routingContext -> {
      LOG.debug("protocol: " + routingContext.request().version());
    });*/

    if (envType.equals(AppConstants.SRVC1)) {
      router
          .route(HttpMethod.GET, "/javamicro/srvc1")
          .handler(
              (routingContext) ->
                  routingContext
                      .response()
                      .end(Buffer.buffer(new JsonObject().put("response_code", 100).toString())));
      /*} else if (envType.equals("srvc2")) {*/

      router
          .route(HttpMethod.GET, "/javamicro/srvc2")
          .handler(
              (routingContext) -> {

                // here we will call a backend service through the envoy proxy
                // our envoy proxy is available at 127.0.0.1:9999
                LOG.debug("Calling envoy proxy to call backend service");
                httpClient
                    .getAbs(config.getString(AppConstants.ENVOY_LISTENER_URL_BE_SRVC2))
                    .handler(
                        res -> {
                          LOG.debug("Received initial response from envoy .. ");
                          res.bodyHandler(
                              buf -> {
                                LOG.debug("Received response (via envoy) - {}", buf.toString());
                                routingContext.response().end(buf);
                              });
                        })
                    .end();
              });
    } else if (envType.equals(AppConstants.BE_SRVC_2)) {
      router
          .route(HttpMethod.GET, "/javamicro/be/srvc2")
          .handler(
              (routingContext) -> {
                LOG.debug("Processing request -  {}", routingContext.request().absoluteURI());
                try {
                  routingContext
                      .response()
                      .end(
                          Buffer.buffer(
                              new JsonObject()
                                  .put("response_code", 200)
                                  .put(
                                      "local_ep",
                                      routingContext
                                          .request()
                                          .connection()
                                          .localAddress()
                                          .toString())
                                  .put(
                                      "remote_ep",
                                      routingContext
                                          .request()
                                          .connection()
                                          .remoteAddress()
                                          .toString())
                                  .put(
                                      "service_call_count",
                                      backEndServiceCallCount.incrementAndGet())
                                  .put(
                                      "processing_node",
                                      InetAddress.getLocalHost().getHostName()
                                          + "/"
                                          + InetAddress.getLocalHost().getHostAddress())
                                  .encodePrettily()));
                } catch (UnknownHostException e) {
                  e.printStackTrace();
                }
              });
    }
  }
}
