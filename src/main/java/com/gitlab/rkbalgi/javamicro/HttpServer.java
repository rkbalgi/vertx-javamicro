package com.gitlab.rkbalgi.javamicro;

import com.gitlab.rkbalgi.mockhsm.MockThalesHsm;
import com.google.common.collect.Lists;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpVersion;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.config.ConfigRetriever;
import io.vertx.reactivex.core.Context;
import io.vertx.reactivex.core.Future;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.ext.web.Router;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpServer {

  private static final Logger LOG = LogManager.getLogger(HttpServer.class);
  private static final Vertx vertx =
      Vertx.vertx(new VertxOptions().setPreferNativeTransport(true).setWorkerPoolSize(4));

  public static void main(String[] args) {
    //

    JsonObject config =
        ConfigRetriever.create(
            vertx,
            new ConfigRetrieverOptions()
                .addStore(
                    new ConfigStoreOptions()
                        .setType("file")
                        .setConfig(new JsonObject().put("path", AppConstants.APP_CONFIG_FILE)))
                .addStore(new ConfigStoreOptions().setType("env")))
            .rxGetConfig()
            .blockingGet();

    LOG.debug("Retrieved application config - {}", config.encodePrettily());

    HttpServerOptions serverOptions = new HttpServerOptions().setPort(8080).setSsl(false);
    /*serverOptions =
        config.getBoolean(AppConstants.USE_HTTP2, false) ? serverOptions.setUseAlpn(true)
            : serverOptions.setUseAlpn(false);
    */

    io.vertx.reactivex.core.http.HttpServer httpServer =
        vertx.createHttpServer(serverOptions);

    Router router = Router.router(vertx);

    Context context = vertx.getOrCreateContext();
    context.put(AppConstants.APP_CONFIG, config);

    context.runOnContext(
        (v) -> {
          Future<Object> future = Future.future();
          future.setHandler(
              (result) -> {
                if (result.succeeded()) {
                  new HttpRouteBuilder(router).addRoutes();
                  LOG.debug("Starting HttpServer .. ");
                  httpServer
                      .requestHandler(router::accept)
                      .rxListen()
                      .subscribe(
                          (res) -> LOG.info("HttpServer started OK"),
                          (err) -> LOG.error("Failed to start HttpServer", err));
                }
              });

          if (config.getBoolean("deploy_mock_hsm", false)) {
            new MockThalesHsm().deploy(vertx, future);
          } else {
            LOG.info(() -> "MockHsm will not deployed ..");
            future.complete();
          }
        });
  }
}
