package com.gitlab.rkbalgi.mockhsm;

import io.vertx.core.DeploymentOptions;
import io.vertx.reactivex.core.Future;
import io.vertx.reactivex.core.Vertx;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MockThalesHsm {

  public static final Logger log = LogManager.getLogger(MockThalesHsm.class);
  // private static final Vertx vertx = Vertx.vertx();

  // public static void main(String[] args) {}

  public void deploy(Vertx vertx, Future<Object> future) {
    vertx.deployVerticle(
        MockHsmVerticle.class.getName(),
        new DeploymentOptions(),
        res -> {
          if (res.succeeded()) {
            log.info("Hsm verticle deployed OK");
            future.complete();
          } else {
            log.error("HSM verticle deployment error", res.cause());
            future.fail(res.cause());
          }
        });
  }
}
