package com.gitlab.rkbalgi.javamicro;

import io.vertx.core.Context;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpVersion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class HttpClientTest {

  private static final Logger LOG = LogManager.getLogger(HttpClientTest.class);

  public static void main(String[] args) {

    Vertx vertx = Vertx.vertx();
    HttpClient httpClient = vertx
        .createHttpClient(
            new HttpClientOptions().setHttp2MaxPoolSize(5).setHttp2MultiplexingLimit(1)
                .setProtocolVersion(
                    HttpVersion.HTTP_2));

    Context context = vertx.getOrCreateContext();

    context.runOnContext((v) -> {

      vertx.setPeriodic(200, l -> {
        httpClient.getAbs("http://localhost:8080/javamicro/srvc1", res -> {

          res.bodyHandler(buf -> {
            LOG.debug(res.request().connection().localAddress() + " ->" + buf.toJsonObject());
          });

        }).end();
      });

    });

    context.runOnContext((v) -> {

      vertx.setPeriodic(200, l -> {
        httpClient.getAbs("http://localhost:8080/javamicro/srvc1", res -> {

          res.bodyHandler(buf -> {
            LOG.debug(res.request().connection().localAddress() + " ->" + buf.toJsonObject());
          });

        }).end();
      });

    });

    context.runOnContext((v) -> {

      vertx.setPeriodic(200, l -> {
        httpClient.getAbs("http://localhost:8080/javamicro/srvc1", res -> {

          res.bodyHandler(buf -> {
            LOG.debug(res.request().connection().localAddress() + " ->" + buf.toJsonObject());
          });

        }).end();
      });

    });

    context.runOnContext((v) -> {

      vertx.setPeriodic(100, l -> {
        httpClient.getAbs("http://localhost:8080/javamicro/srvc1", res -> {

          res.bodyHandler(buf -> {
            LOG.debug(res.request().connection().localAddress() + " ->" + buf.toJsonObject());
          });

        }).end();
      });

    });

    context.runOnContext((v) -> {

      vertx.setPeriodic(100, l -> {
        httpClient.getAbs("http://localhost:8080/javamicro/srvc1", res -> {

          res.bodyHandler(buf -> {
            LOG.debug(res.request().connection().localAddress() + " ->" + buf.toJsonObject());
          });

        }).end();
      });

    });


  }

}