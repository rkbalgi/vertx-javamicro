#!/bin/bash

echo "Starting envoy proxy ..."
/usr/local/bin/envoy --v2-config-only -l info -c /etc/envoy/envoy.yaml &
echo "Starting Vert.x HTTP services .. "
java -cp .:javamicro-1.0-SNAPSHOT-jar-with-dependencies.jar com.gitlab.rkbalgi.javamicro.HttpServer

